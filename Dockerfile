
FROM node:boron

WORKDIR /app


COPY package.json /app
RUN npm install

RUN npm install

RUN npm install body-parser
RUN npm install fatsecret

COPY . /app

EXPOSE 8080
CMD [ "npm", "start" ]