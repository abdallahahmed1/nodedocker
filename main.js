var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var bodyParser = require('body-parser');
var urlencoded = bodyParser.urlencoded({extended: false});

const FatSecret = require('fatsecret');
const fatAPI =  new FatSecret('156778de2a9c4f349064156ca0e4b296', '94c7f9e2ca454bb6ba86c5d2d8be76cc');



app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.render('index');
});

app.post('/searchFood', urlencoded, function (req, res) {

fatAPI.method('foods.search', {
        search_expression: req.body.food,
        max_results: 1
    })
    .then(function(results) {
        if(results.foods.food !== undefined)
            res.render('response', {food: results.foods.food.food_description});
        else{
            res.render('response', {food: "Enter valid food"});
        }
    });
})
app.listen(port, function() {

});
